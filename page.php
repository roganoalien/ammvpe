<?php get_template_part('include/header') ?>

<?php while( have_posts() ) : the_post(); ?>
	<?php if(is_front_page()) { ?>
		<header class="main-header">
			<section class="container">
				<article class="row">
					<h1 class="col-12 offset-md-2 col-md-8"><?php echo the_title(); ?></h1>
				</article>
				<article class="row">
					<div class="col-12 offset-md-2 col-md-8"><?php the_content(); ?></div>
				</article>
			</section>
			<div class="floating-image" style="background: url(<?php the_post_thumbnail_url(); ?>) no-repeat;"></div>
			<nav class="navigation-space">
				<div class="container">
					<div class="row">
						<?php 
							$args = array(
								'container' => 'div',
								'container_class' => 'col-12',
								'container_id' => 'home-menu',
								'menu' => 'home-menu',
								'theme_location' => 'home-menu',
							);
							wp_nav_menu($args);
						?>
					</div>
				</div>
			</nav>
		</header>
	<?php } else{ ?>
		<header class="main-header not-front" <?php if(!is_front_page()){ ?> style="background: url(<?php the_post_thumbnail_url(); ?>) no-repeat;" <?php } ?>>
			<div class="gradient"></div>
			<section class="container">
				<article class="row">
					<h1 class="col-sm-12 offset-md-2 col-md-8">
						<?php echo the_title(); ?>
					</h1>
				</article>
			</section>
			<nav class="navigation-space">
				<div class="container">
					<div class="row">
						<?php 
							$args = array(
								'container' => 'div',
								'container_class' => 'col-12',
								'container_id' => 'main-menu',
								'menu' => 'main-menu',
								'theme_location' => 'main-menu',
							);
							wp_nav_menu($args);
						?>
					</div>
				</div>
			</nav>
		</header>
		<section class="main-content page-content">
			<article class="container">
				<div class="row justify-content-center">
					<div class="col-10 col-auto">
						<?php bootstrap_breadcrumbs(); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<?php the_content(); ?>
					</div>
				</div>
			</article>
		</section>
	<?php } ?>
<?php endwhile; ?>

<?php if(is_front_page()) {?>
	<section class="main-content home-content">
		<article class="container">
			<div class="row justify-content-center">
				<div class="col-10 col-auto">
					<?php bootstrap_breadcrumbs(); ?>
				</div>
			</div>
			<div class="row">
				<div class="magazine col-12 col-md-6 col-lg-5 offset-lg-1">
					<div class="swiper-container square-container">
						<h2 class="square-title">Veterinaria Clínica</h2>
						<div class="swiper-pagination"></div>
						<div class="arrow-left arrow">
							<svg width="48" height="48" viewBox="0 0 48 48"><title>ic keyboard arrow left 48px</title>
								<g fill="#353535">
									<path d="M30.83 32.67l-9.17-9.17 9.17-9.17L28 11.5l-12 12 12 12z"></path>
								</g>
							</svg>
						</div>
						<div class="arrow-right arrow">
							<svg width="48" height="48" viewBox="0 0 48 48"><title>ic keyboard arrow right 48px</title>
								<g fill="#353535">
									<path d="M17.17 32.92l9.17-9.17-9.17-9.17L20 11.75l12 12-12 12z"></path>
								</g>
							</svg>
						</div>
						<div class="swiper-wrapper">
							<?php
								$args_ciencia = array('post_type' => 'cientificas', 'posts_per_page' => 12);
								$loop = new WP_Query($args_ciencia);
							?>
							<?php while($loop->have_posts()) : $loop->the_post(); ?>
								<div class="swiper-slide" style="background: url(<?php echo get_field('magazine-img'); ?>) no-repeat;">
									<a href="<?php echo get_field('magazine-file', false, false); ?>"><h3><?php echo get_field('magazine-title'); ?></h3></a>
									<div class="slide-gradient"></div>
								</div>
							<?php endwhile; ?>
						</div>
					</div>
				</div>
				<div class="magazine col-12 col-md-6 col-lg-5">
					<div class="swiper-container square-container">
						<h2 class="square-title">veTIP <span>Consejo a propietarios</span></h2>
						<div class="swiper-pagination"></div>
						<div class="arrow-left arrow">
							<svg width="48" height="48" viewBox="0 0 48 48"><title>ic keyboard arrow left 48px</title>
								<g fill="#353535">
									<path d="M30.83 32.67l-9.17-9.17 9.17-9.17L28 11.5l-12 12 12 12z"></path>
								</g>
							</svg>
						</div>
						<div class="arrow-right arrow">
							<svg width="48" height="48" viewBox="0 0 48 48"><title>ic keyboard arrow right 48px</title>
								<g fill="#353535">
									<path d="M17.17 32.92l9.17-9.17-9.17-9.17L20 11.75l12 12-12 12z"></path>
								</g>
							</svg>
						</div>
						<div class="swiper-wrapper">
							<?php
								$args_propietaria = array('post_type' => 'propietarias', 'posts_per_page' => 12);
								$loop = new WP_Query($args_propietaria);
							?>
							<?php while($loop->have_posts()) : $loop->the_post(); ?>
								<div class="swiper-slide" style="background: url(<?php echo get_field('magazine-img'); ?>) no-repeat;">
									<a href="<?php echo get_field('magazine-file', false, false); ?>"><h3><?php echo get_field('magazine-title'); ?></h3></a>
									<div class="slide-gradient"></div>
								</div>
							<?php endwhile; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-6 col-lg-10 offset-lg-1">
					<p class="fake-title">Clínicas de Confianza de Nuestros Socios</p>
				</div>
				<div class="col-12 col-md-6 col-lg-10 offset-lg-1">
					<div class="square-container map" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/mapa.png) no-repeat center center;">
						<h2 class="square-title">Clínica Veterinaria UMSNH</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-6 col-lg-10 offset-lg-1">
					<div class="square-container event">
						<h2 class="square-title">Próximos Eventos</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-6 col-lg-5 offset-lg-1">
					<div class="square-container vacantes">
						<h2 class="square-title">Bolsa de Trabajo</h2>
						<?php
							$args_vacantes = array('post_type' => 'vacantes', 'posts_per_page' => 12);
							$loop = new WP_Query($args_vacantes);
						?>
						<?php while($loop->have_posts()) : $loop->the_post(); ?>
							<div class="vacante">
								<div class="vacante-header">
									<h3><?php echo get_field('title'); ?></h3>
									<?php $jornada = get_field('vacante-jornada'); ?>
									<?php foreach($jornada as $value){ ?>
										<span class="badge badge-success"><?php echo $value ?></span>
									<?php } ?>
								</div>
								<div class="vacante-place">
									<div class="first">
										<svg width="24" height="24" viewBox="0 0 24 24"><title>ic business 24px</title>
											<g fill="#353535">
												<path d="M12 7V3H2v18h20V7H12zM6 19H4v-2h2v2zm0-4H4v-2h2v2zm0-4H4V9h2v2zm0-4H4V5h2v2zm4 12H8v-2h2v2zm0-4H8v-2h2v2zm0-4H8V9h2v2zm0-4H8V5h2v2zm10 12h-8v-2h2v-2h-2v-2h2v-2h-2V9h8v10zm-2-8h-2v2h2v-2zm0 4h-2v2h2v-2z"></path>
											</g>
										</svg>
										<p><?php echo get_field('vacante-job-place'); ?></p>
									</div>
									<div class="second">
										<svg width="24" height="24" viewBox="0 0 24 24"><title>ic location on 24px</title>
											<g fill="#0085DC">
												<path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"></path>
											</g>
										</svg>
										<p><?php echo get_field('vacante-place'); ?></p>
									</div>
								</div>
								<div class="vacante-date">
									<p><?php the_date('F j, Y'); ?></p>
								</div>
								<div class="vacante-contact">
									<p>Contacto</p>
									<a href="mailto:<?php echo get_field('vacante-email'); ?>"><?php echo get_field('vacante-email'); ?></a>
									<a href="tel:<?php echo get_field('vacante-phone'); ?>"><?php echo get_field('vacante-phone'); ?></a>
								</div>
								<div class="vacante-description">
									<?php echo get_field('vacante-description'); ?>
								</div>
								<div class="slide-gradient"></div>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-5">
					<div class="square-container videos swiper-container">
						<h2 class="square-title">Videos AMMVDPE</h2>
						<div class="square-play">
							<svg width="24" height="24" viewBox="0 0 24 24"><title>ic play circle filled 24px</title>
								<g fill="#fff">
									<path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path>
								</g>
							</svg>
						</div>
						<div class="swiper-pagination"></div>
						<div class="arrow-left arrow">
							<svg width="48" height="48" viewBox="0 0 48 48"><title>ic keyboard arrow left 48px</title>
								<g fill="#353535">
									<path d="M30.83 32.67l-9.17-9.17 9.17-9.17L28 11.5l-12 12 12 12z"></path>
								</g>
							</svg>
						</div>
						<div class="arrow-right arrow">
							<svg width="48" height="48" viewBox="0 0 48 48"><title>ic keyboard arrow right 48px</title>
								<g fill="#353535">
									<path d="M17.17 32.92l9.17-9.17-9.17-9.17L20 11.75l12 12-12 12z"></path>
								</g>
							</svg>
						</div>
						<div class="swiper-wrapper">
							<?php
								$args_video = array('post_type' => 'videos', 'posts_per_page' => 12);
								$loop = new WP_Query($args_video);
							?>
							<?php while($loop->have_posts()) : $loop->the_post(); ?>
								<a href="<?php echo get_field('youtube_url'); ?>" class="swiper-slide mediabox" style="background: url(<?php echo get_field('thumbnail_video'); ?>) no-repeat;">
									<h3><?php echo get_field('video_title'); ?></h3>
									<div class="slide-gradient"></div>
								</a>
							<?php endwhile; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-6 col-lg-10 offset-lg-1">
					<div class="ad-container" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/ad.png) no-repeat center center;"></div>
				</div>
			</div>
		</article>
	</section>
<?php } ?>

<?php get_template_part('include/footer') ?>