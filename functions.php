<?php 
//theme support for creating menu called header
function register_my_menu() {
    register_nav_menu('main-menu',__( 'Main Menu' ));
    register_nav_menu('home-menu',__( 'Home Menu' ));
}
add_action( 'init', 'register_my_menu' );
//theme support for posts thumbnails
add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-formats', array( 'image', 'chat', 'video', 'gallery' ) );
//REMOVE P TAGS AROUND IMAGES ON POSTS
function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

// bootstrap breadcrumbs
function bootstrap_breadcrumbs(){
    if( !is_home() && !is_front_page() ){
        echo '<ul class="breadcrumb">';
        echo '<li class="breadcrumb-item"><a href="'.home_url('/').'">Inicio</a></li>';
        if(is_category() || is_single()){
            //cuando está dentro de una categoría o algo
        } elseif (is_page()){
            echo '<li class="breadcrumb-item active">'.the_title('', '', FALSE).'</li>';
        }
        echo '</ul>';
    } else{
        echo '<ul class="breadcrumb">';
        echo '<li class="breadcrumb-item active">Inicio</li>';
        echo '</ul>';
    }
}

// custom post type
add_action('init', 'create_magazines');
function create_magazines(){
    $labels = array(
        'name' => __('Clínicas'),
        'singular_name' => __('Clínica')
    );
    $args = array(
        'labels' => $labels,
        'description' => __('Revistas Clínicas'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-book',
        'map_meta_cap' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'revistas-cientificas'
        ),
        'supports' => array(
            'title', 'thumbnail', 'excerpt', 'custom-fields', 'page-attributes', 'post-formats', 'editor'
        )
    );
    register_post_type('cientificas', $args);

    $labels_two = array(
        'name' => __('veTips'),
        'singular_name' => __('veTip')
    );
    $args_two = array(
        'labels' => $labels_two,
        'description' => __('Revistas Propietarias'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-book',
        'map_meta_cap' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'revistas-propietarias'
        ),
        'supports' => array(
            'title', 'thumbnail', 'excerpt', 'custom-fields', 'page-attributes', 'post-formats', 'editor'
        )
    );
    register_post_type('propietarias', $args_two);

    $labels_three = array(
        'name' => __('Videos'),
        'singular_name' => __('Video')
    );
    $args_three = array(
        'labels' => $labels_three,
        'description' => __('Videos que salen en Home'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 7,
        'menu_icon' => 'dashicons-video-alt3',
        'map_meta_cap' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'videos-home'
        ),
        'supports' => array(
            'title', 'thumbnail', 'excerpt', 'custom-fields', 'page-attributes', 'post-formats', 'editor'
        )
    );
    register_post_type('videos', $args_three);

    $labels_four = array(
        'name' => __('Vacantes'),
        'singular_name' => __('Vacante')
    );
    $args_four = array(
        'labels' => $labels_four,
        'description' => __('Vacantes de Trabajo'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 8,
        'menu_icon' => 'dashicons-category',
        'map_meta_cap' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'vacantes'
        ),
        'supports' => array(
            'title', 'thumbnail', 'excerpt', 'custom-fields', 'page-attributes', 'post-formats', 'editor'
        )
    );
    register_post_type('vacantes', $args_four);
}
/******************************************
 * REMOVE ITEMS FROM SIDEMENU BAR - ADMIN *
 ******************************************/
function custom_menu_page_removing(){
    // Appearance
    remove_menu_page('themes.php');
    // Comments
    remove_menu_page('edit-comments.php');
    // Media
    remove_menu_page('upload.php');
    // Plugins
    remove_menu_page('plugins.php');
    // Tools
    remove_menu_page('tools.php');
    // Users
    remove_menu_page('users.php');
}
add_action('admin_menu', 'custom_menu_page_removing');
?>