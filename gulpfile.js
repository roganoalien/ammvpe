const	gulp		= require('gulp'),
		autoprefixer= require('gulp-autoprefixer'),
		concat		= require('gulp-concat'),
		del			= require('del'),
		log			= require('./logger'),
		minify		= require('gulp-minify'),
		rename		= require('gulp-rename'),
		sass		= require('gulp-sass'),
		sourcemaps	= require('gulp-sourcemaps');

/********
 * SASS *
 ********/

gulp.task('sass', () => {
	return gulp.src('./sass/main.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: 'compressed'
		})).on('error', sass.logError)
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: true
		}))
		.pipe(rename('main.min.css'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./css'))
		.on('end', ()=>{
			log('Sass Compilado', 'purple');
		});
});

/******
 * JS *
 ******/

gulp.task('js', ()=>{
	let scripts = './scripts/dev';
	return gulp.src([`${ scripts }/index.js`])
		.pipe(sourcemaps.init())
		.pipe(concat('main.js'))
		.pipe(minify({ext:{src:'-min.js', min:'.js'}}))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('./scripts'))
		.on('end', ()=>{
			log('JS Compilado', 'yellow');
		});
});

gulp.task('js-delete-min', ()=>{
	return del(['./scripts/main-min.js', './scripts/main-min.js.map']).then(paths => {
		if(paths.length == 0){
			log('No se borró ningún archivo', 'red');
		} else{
			log(`Se eliminó:\n${ paths.join('\n') }`, 'green');
		}
	});
})

/************
 * WATCHERS *
 ************/

gulp.task('watchers', (done)=>{
	log('Watchers Corriendo', 'yellow');
	gulp.watch('./sass/**/*.scss', gulp.series('sass'));
	gulp.watch('./scripts/dev/**/*.js', gulp.series('js', 'js-delete-min'));
	done();
});

gulp.task('dev', gulp.series('sass', gulp.parallel('watchers')));