	<script src="<?php echo get_template_directory_uri(); ?>/scripts/vendor/swiper.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/scripts/vendor/mediabox.min.js"></script>
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			MediaBox('.mediabox');
			var _$swiper = new Swiper('.swiper-container', {
				effect: 'fade',
				direction: 'horizontal',
				loop: true,
				pagination: {
					el: '.swiper-pagination'
				},
				navigation: {
					nextEl: '.arrow-right',
					prevEl: '.arrow-left'
				}
			});
		});
	</script>
	</body>
</html>