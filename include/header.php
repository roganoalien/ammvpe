<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="<?php bloginfo('description') ?>">
	<meta name="robots" content="index, follow">
	<meta name="author" content="Rodrigo García">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?php if(is_front_page()){ ?>
		<title><?php bloginfo('name'); ?></title>
	<?php } else{ ?>
		<title><?php the_title(); ?> | AMMVDPE</title>
	<?php } ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/vendors/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/vendors/swiper.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/vendors/mediabox.min.css">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
</head>
<body>